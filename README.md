# `SDDM` sweet theme

`SDDM` sweet theme by [Eliver Lara](https://github.com/EliverLara).

Copy [sweet](sweet) into `/usr/share/sddm/themes` and write this:

```conf
[Theme]
Current=sweet
```

inside `/etc/sddm.conf.d`.
